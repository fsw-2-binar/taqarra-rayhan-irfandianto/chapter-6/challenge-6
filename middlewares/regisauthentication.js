const jwt = require('jsonwebtoken')
const jwtDecode=require('jwt-decode')
const regisJWT = (req, res, next) => {
    try{
        const authHeader = req.headers['authorization']

    if(authHeader){
        const token = authHeader.split(' ')[0]
        const secretKey = process.env.SECRETKEY
        const decoded =jwtDecode(token)
        let role=decoded.role

        let roleP=req.params.role
        console.log("ini param rle",roleP)
        console.log(role)
        if(roleP === "member"){
            
                res.status(401).json({
                    message: 'anda bukan super admin atau admin'
                })
           
            
        } else if (roleP === "admin"){
            if (role === 'super admin') {
                next()
            } else {
                res.status(401).json({
                    message: 'Anda bukan super admin'
                })
            }

        }
        
    }else{
        res.sendStatus(403)
    }
    }
    catch(error){
        res.status(401).json({
            message: 'Tidak Memiliki Akses'
        })
        
    }
    
}

module.exports = regisJWT


// // import json jwt decode
// // const jwt_decode = require('jwt-decode')

// const RoleJWT = (req, res, next) => {
//     try {
//         const authHeader = req.headers['authorization']

//         // console.log(req.headers)

//         if (authHeader) {
//             const tokenPlus = authHeader.split('rayhan ')[1]

//             // decode token
//             const decoded = jwtDecode(tokenPlus)
//             let role = decoded.role
//             console.log("ini role",role)
//             if (role === 'admin' || role === 'super admin') {
//                 next()
//             } else {
//                 res.status(401).json({
//                     message: 'Anda Bukan Rolenya!'
//                 })
//             }
//         } else {
//             res.sendStatus(403)
//         }

//     } catch (error) {
//         res.status(401).json({
//             message: 'Tidak Memiliki Akses'
//         })
//     }
// }

// // export
// module.exports = RoleJWT
