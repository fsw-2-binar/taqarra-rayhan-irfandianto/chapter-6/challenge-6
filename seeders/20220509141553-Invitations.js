'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('tbl_invitations', [
      {
        name: 'irfan',
        place: 'malang',
        is_read: true,
        email:'irfan@gmail.com',
        contact:'08111122223333',
        is_attendance:true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'rayhan',
        place: 'bogor',
        is_read: false,
        email:'rayhan@gmail.com',
        contact:'08111122224444',
        is_attendance:false,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'anton',
        place: 'banten',
        is_read: true,
        email:'anton@gmail.com',
        contact:'08111122225555',
        is_attendance:false,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
