'use strict';
const bcrypt = require('bcryptjs')
module.exports = {
  async up (queryInterface, Sequelize) {
    let salt ='AbuIcdD'
    const hashPassword = await bcrypt.hash("admin123"+salt,12)
    await queryInterface.bulkInsert('tbl_users', [
     {
       name:"Rayhan Irfan",
       email:"super.admin@gmail.com",
       role:"super admin",
       password:hashPassword,
       createdAt : new Date(),
       updatedAt : new Date()
     }
    ], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
