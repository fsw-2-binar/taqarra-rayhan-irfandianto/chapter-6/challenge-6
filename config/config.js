require('dotenv').config()

module.exports = {
    "development": {
      "username": process.env.DB_USERNAME_DEV,
      "password": process.env.DB_PASSWORD_DEV,
      "database": process.env.DB_NAME_DEV,
      "host": "127.0.0.1",
      "dialect": process.env.DB_DIALECT_DEV
    },
    "test": {
      "username": process.env.DB_USERNAME_TEST,
      "password": process.env.DB_PASSWORD_TEST,
      "database": process.env.DB_NAME_TEST,
      "host": "127.0.0.1",
      "dialect": process.env.DB_DIALECT_TEST
    },
    "production": {
      "username": process.env.DB_USERNAME_PROD,
      "password": process.env.DB_PASSWORD_PROD,
      "database": process.env.DB_NAME_PROD,
      "host": "127.0.0.1",
      "dialect": process.env.DB_DIALECT_PROD
    }
  }
  