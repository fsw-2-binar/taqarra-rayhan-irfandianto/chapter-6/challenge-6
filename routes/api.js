var express = require('express');


const {login} = require ('../controllers/auth.controller')
const {registerUser,editUser,getAllUser,getUserById,deleteUser,getUserByToken}=require('../controllers/user.controller')
const {createCars,editCars,deleteCars,getData,getcarsById,getDataAvailable}= require('../controllers/mobil.controller')


const authJWT = require('../middlewares/jwtauthenticate');
const RoleJWT = require('../middlewares/roleauthentication');
const regisJWT = require ('../middlewares/regisauthentication')
var router = express.Router();




router.post('/v1/user/create/admin/:role',[authJWT,regisJWT],registerUser);
router.post('/v1/user/create/member/:role',registerUser);
router.get('/v1/user/profile',[authJWT],getUserByToken);
router.get('/v1/user/list',[authJWT,RoleJWT],getAllUser)
// router.put('/v1/user/edit/:id',[authJWT,RoleJWT],editUser);
// router.delete('/v1/user/delete/:id',[authJWT,RoleJWT],deleteGallery);

router.post('/v1/cars/create',[authJWT,RoleJWT],createCars);
router.get('/v1/cars/list',[authJWT],getDataAvailable)
// router.get('/v1/cars/list/:id',[authJWT,RoleJWT],getcarsById)
router.get('/v1/cars/admin/list',[authJWT,RoleJWT],getData)
router.get('/v1/cars/list/:id',[authJWT,RoleJWT],getcarsById)

router.put('/v1/cars/edit/:id',[authJWT,RoleJWT],editCars)
router.delete('/v1/cars/delete/:id',[authJWT,RoleJWT],deleteCars)



router.post('/v1/user/login',login);
router.post('/v1/admin/login',login);


// router.post('/v1/user/login',login)


module.exports = router;