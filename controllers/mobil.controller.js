const {tbl_cars}= require('../models')
const jwtDecode=require('jwt-decode')
const { Sequelize } = require('sequelize');
const Op = Sequelize.Op
async function createCars(req,res){
    try{
        const authHeader = req.headers['authorization']
        const token = authHeader.split(' ')[0]
        // const secretKey = process.env.SECRETKEY
        const decoded =jwtDecode(token)
        let nameToken=decoded.name
        
        let dataReq = {
          name : req.body.name,
          cost : req.body.cost,
          size : req.body.size,
          url_image: req.body.url_image,
          createdBy: nameToken,
         

          createdAt : new Date(),
          updatedAt : new Date(),
            
      
     
   
          }
  
          tbl_cars.create(dataReq).then((data)=>{
            res.status(200).json({
              message: 'success'
            })
          }).catch((error)=> {
            res.status(500).json({
              message: error.message
            })
          })
    }catch(error){
        res.status(500).send({
       
            message:error.message
          })
    }
  }

  async function getDataAvailable(req, res){
    try{
        const mobil= await tbl_cars.findAll({
          where:{
            deletedBy: null,
            deletedAt: null
          }
        });
        
        console.log(mobil)
        res.status(200).json({
            message:'success',
            data: mobil
              })
      }
      
      catch(error){
         
      
      }

}

async function getData(req, res){
  try{
      const mobil= await tbl_cars.findAll();
      
      console.log(mobil)
      res.status(200).json({
          message:'success',
          data: mobil
            })
    }
    
    catch(error){
       
    
    }

}


// async function getData(req, res) {
//   try {
//       const gallery = await models.tbl_gallery.findAll({})

//       res.status(200).json({
//           message: "get all gallery success",
//           data: gallery
//       })
//   } catch (error) {
//       res.status(500).json({
//           message: "get all gallery failed",
//           data: []
//       })
//   }
// }

async function deleteCars(req, res)  {
  try{
   
      const authHeader = req.headers['authorization']
        const token = authHeader.split(' ')[0]
        // const secretKey = process.env.SECRETKEY
        const decoded =jwtDecode(token)
        let nameToken=decoded.name

      await tbl_cars.update({
      
        deletedBy:nameToken,
        deletedAt:new Date()
        
       }, {
         where:{
           id:req.params.id,
           deletedAt:null,
           deletedBy:null
         }
        }).then(() => {
        res.status(200).json({
          message: 'success'
        })
        // res.send({name : "StackOverFlow", reason : "Need help!", redirect_path: "/"});
        })
    }
    
    catch(error){
       
      res.status(500).send({
     
        message:error.message
      })
    }

}


async function editCars(req,res){
  try{

    const query = {
        where: { id:req.params.id,
          deletedAt:null,
           deletedBy:null
           }
       }
       const authHeader = req.headers['authorization']
       const token = authHeader.split(' ')[0]
       // const secretKey = process.env.SECRETKEY
       const decoded =jwtDecode(token)
       let nameToken=decoded.name
    await tbl_cars.update({

          name : req.body.name,
          cost : req.body.cost,
          size : req.body.size,
          url_image: req.body.url_image,
          updatedAt:new Date(),
          updatedBy: nameToken
       }, query).then(() => {
        res.status(200).json({
          message: 'success'
        })
        // res.send({name : "StackOverFlow", reason : "Need help!", redirect_path: "/"});
        })
   
    
  }
  
  catch(error){
     
    res.status(500).send({
   
      message:error.message
    })
  }
}

async function getcarsById(req,res){
  try{
      const userId = await tbl_cars.findOne({
        where : {id : req.params.id}
      });
      
      res.status(200).json({
          message:"success",
          data: userId
            })
    }
    
    catch(error){
       
      res.status(500).send({
     
        message:error.message
      })
    }
}


  module.exports = {
    
    createCars,
    getData,
    getDataAvailable,
    deleteCars,
    getcarsById,
    editCars
    
    
}