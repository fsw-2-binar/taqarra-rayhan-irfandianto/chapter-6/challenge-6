const {tbl_users}= require('../models')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
async function registerUser(req,res){
    try 
    {

      let salt ='AbuIcdD'
      const hashPassword = await bcrypt.hash(req.body.password+salt,12)
        let dataReq = {
          name : req.body.name,
          email : req.body.email,
          password :hashPassword,
          createdAt : new Date(),
          updatedAt : new Date()
        }

        tbl_users.create(dataReq).then((data)=>{
          res.status(200).json({
            message: 'success'
          })
        }).catch((error)=> {
          res.status(500).json({
            message: error.message
          })
        })
        
      
      }
      
      catch(error){
         
        res.status(500).send({
       
          message:error.message
        })
      }
}

async function authUser(req,res){
  try{
    const body =req.body
    const user= await tbl_users.findOne({
      where: {
        email:body.email
      }
    })

    let salt ='AbuIcdD'
    
    const matchPass = await bcrypt.compare(body.password+salt, user.password)
   
    if(!matchPass){
      return res.status(401).json({
        message:"email/password not match"
      })
    }
    const secretKey= '2761a8e7612d7ed84042f1bacd056f6da0b81cac51a62f93106c28f7a26ff4c1'
    const expireKey='1h'

  //   const tokenAccess = jwt.sign(
  //     // Define object information in JWT Sign
  //     // tidak boleh mendefinisikan password pada token
  //     {
  //         id:user.id,
  //         email:user.email,
  //         name:user.name
  //     },
  //     // Define secretkey
  //     secretKey,
  //     // Define options
  //     {
  //         //algorithm untuk membantu proses hashing data
  //         algorithm:"HS256",
  //         expiresIn: expireKey
  //     }
  // )

  const tokenAccess= jwt.sign(
    {
        id:user.id,
        email:user.email,
        name:user.name
    },
    
    secretKey,
    
    {
      algorithm:"HS256",
      
      // expireIn: expireKey
      // algorithm:"HS256",
      expireIn: expireKey

    }


      //   secretKey,
      // // Define options
      // {
      //     //algorithm untuk membantu proses hashing data
      //     algorithm:"HS256",
      //     expiresIn: expireKey
      // }
  
  )

  
    return res.status(200).json({
      message:"successs",
      token:tokenAccess
    })
  } 
  catch(error){

  }
}

async function getAllUser(req, res){
  try {
      const users = await tbl_users.findAll()

      res.status(200).json({
          message:'success',
          data : users
      })
  } catch (error) {
      console.log(error);
  }
}


module.exports = {
    registerUser,
    authUser,
    getAllUser
}