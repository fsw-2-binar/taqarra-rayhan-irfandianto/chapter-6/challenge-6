const {tbl_user}= require('../models')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

async function login(req,res){
    try{
        const body =req.body
        const user= await tbl_user.findOne({
          where: {
            email:body.email
          }
        })
    
        let salt ='AbuIcdD'
        
        const matchPass = await bcrypt.compare(body.password+salt, user.password)
        if(!matchPass){
            return res.status(401).json({
              message:"email/password not match"
            })
          }
        const jwtKey = process.env.SECRETKEY
        const expireKey= process.env.EXPIREKEY
        console.log(jwtKey)
        const tokenAccess= jwt.sign(
            {
                id:user.id,
                email:user.email,
                role:user.role,
                name:user.name
            },
            
            jwtKey,
            
            {
              algorithm:"HS256",
              
              // expireIn: expireKey
              // algorithm:"HS256",
              expiresIn: expireKey
        
            }
        
        
              //   secretKey,
              // // Define options
              // {
              //     //algorithm untuk membantu proses hashing data
              //     algorithm:"HS256",
              //     expiresIn: expireKey
              // }
          
          )
        
    return res.status(200).json({
        message:"successs",
        token:tokenAccess
      })
    } 
    catch(error){
  
    }
    
}

module.exports={
    login
}