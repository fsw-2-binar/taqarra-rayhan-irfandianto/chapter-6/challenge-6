const {tbl_user}= require('../models')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const jwtDecode=require('jwt-decode')
async function registerUser(req,res){
    try 
    {

      let salt ='AbuIcdD'
      const hashPassword = await bcrypt.hash(req.body.password+salt,12)
        let dataReq = {
          name : req.body.name,
          email : req.body.email,
          password :hashPassword,
          role:req.params.role,
          createdAt : new Date(),
          updatedAt : new Date()
        }

        tbl_user.create(dataReq).then((data)=>{
          res.status(200).json({
            message: 'success'
          })
        }).catch((error)=> {
          res.status(500).json({
            message: error.message
          })
        })
        
      
      }
      
      catch(error){
         
        res.status(500).send({
       
          message:error.message
        })
      }
}

// async function authUser(req,res){
//   try{
//     const body =req.body
//     const user= await tbl_users.findOne({
//       where: {
//         email:body.email
//       }
//     })

//     let salt ='AbuIcdD'
    
//     const matchPass = await bcrypt.compare(body.password+salt, user.password)
   
//     if(!matchPass){
//       return res.status(401).json({
//         message:"email/password not match"
//       })
//     }
//     const secretKey= '2761a8e7612d7ed84042f1bacd056f6da0b81cac51a62f93106c28f7a26ff4c1'
//     const expireKey='1h'

//   //   const tokenAccess = jwt.sign(
//   //     // Define object information in JWT Sign
//   //     // tidak boleh mendefinisikan password pada token
//   //     {
//   //         id:user.id,
//   //         email:user.email,
//   //         name:user.name
//   //     },
//   //     // Define secretkey
//   //     secretKey,
//   //     // Define options
//   //     {
//   //         //algorithm untuk membantu proses hashing data
//   //         algorithm:"HS256",
//   //         expiresIn: expireKey
//   //     }
//   // )

//   const tokenAccess= jwt.sign(
//     {
//         id:user.id,
//         email:user.email,
//         name:user.name
//     },
    
//     secretKey,
    
//     {
//       algorithm:"HS256",
      
//       // expireIn: expireKey
//       // algorithm:"HS256",
//       expireIn: expireKey

//     }


//       //   secretKey,
//       // // Define options
//       // {
//       //     //algorithm untuk membantu proses hashing data
//       //     algorithm:"HS256",
//       //     expiresIn: expireKey
//       // }
  
//   )

  
//     return res.status(200).json({
//       message:"successs",
//       token:tokenAccess
//     })
//   } 
//   catch(error){

//   }
// }

async function getAllUser(req, res){
  try {
      const users = await tbl_user.findAll()

      res.status(200).json({
          message:'success',
          data : users
      })
  } catch (error) {
      console.log(error);
  }
}




async function deleteUser(req, res)  {
try{
    await tbl_user.destroy({
      where:{
          id:req.params.id
      }
    }).then(() => {
      res.status(200).json({
        message: 'success'
      })
      // res.send({name : "StackOverFlow", reason : "Need help!", redirect_path: "/"});
   
    })
  }
  
  catch(error){
     
    res.status(500).send({
   
      message:error.message
    })
  }

}


async function editUser(req,res){
try{
  const query = {
      where: { id:req.params.id }
     }
  
  await tbl_user.update({
    name : req.body.name,
    email : req.body.email,
    // password :hashPassword,
    role:req.params.role,
     
     }, query).then(() => {
      res.status(200).json({
        message: 'success'
      })
      // res.send({name : "StackOverFlow", reason : "Need help!", redirect_path: "/"});
      })
 
  
}

catch(error){
   
  res.status(500).send({
 
    message:error.message
  })
}
}


async function getUserById(req,res){
  try{
      const userId = await tbl_user.findOne({
        where : {id : req.params.id}
      });
      
      res.status(200).json({
          message:"success",
          data: userId
            })
    }
    
    catch(error){
       
      res.status(500).send({
     
        message:error.message
      })
    }
}

async function editUser(req,res){
  try{
    const query = {
        where: { id:req.params.id }
       }
    
    await tbl_invitations.update({
        // name: req.body.name,
        // place: req.body.place,
        is_read:req.body.is_read,
        // email:req.body.email,
        // contact:req.body.contact,
        is_attendance:req.body.is_attendance
       }, query).then(() => {
        res.status(200).json({
          message: 'success'
        })
        // res.send({name : "StackOverFlow", reason : "Need help!", redirect_path: "/"});
        })
   
    
  }
  
  catch(error){
     
    res.status(500).send({
   
      message:error.message
    })
  }
}

async function getUserByToken(req,res){
  try{
    const authHeader = req.headers['authorization']
    const token = authHeader.split(' ')[0]

    const decoded =jwtDecode(token)
    let idToken=decoded.id
      const userId = await tbl_user.findOne({
        where : {id : idToken},attributes:{exclude:['password']}
        
      });
      
      res.status(200).json({
          message:"success",
          data: userId
            })
    }
    
    catch(error){
       
      res.status(500).send({
     
        message:error.message
      })
    }
}

module.exports = {
    registerUser,
    editUser,
    getAllUser,
    getUserById,
    deleteUser,
    getUserByToken
}