
## inisialisasi
1. npm init \
2. sequelize db:migrate \
3. sequelize db:seed --seed 20220513043002-SuperAdminSeeder.js    
## Email dan Password Super Admin

Email: super.admin@gmail.com \
Password:admin123

## End Point
1. http://localhost:8080/api/v1/user/create/admin/admin (registrasi admin) 
2. http://localhost:8080/api/v1/user/create/member/member (registrasi member)
3. http://localhost:8080/api/v1/admin/login (login sebagai admin) 
4. http://localhost:8080/api/v1/user/login (login sebagai member) 
5. http://localhost:8080/api/v1/user/list (daftar user hanyak bisa diakses admin/super admin) 
6. http://localhost:8080/api/v1/user/profile (mengecek profile diri berdasarkan token)
7. http://localhost:8080/api/v1/cars/list (menampilkan mobil yang tersedia)
8. http://localhost:8080/api/v1/cars/admin/list(menampilkan semua mobil termasuk yang pernah di delete dapat diakses oleh admin/superadmin saja) 
9. http://localhost:8080/api/v1/cars/delete/5 (delete mobil)
10. http://localhost:8080/api/v1/cars/edit/4 (edit car )
11. http://localhost:8080/api/v1/cars/create (create car)
12. http://localhost:8080/api/v1/cars/list/5 (menampilkan mobil berdasarkan id)


## Documentation API

untuk membuka API dapat mengakses halaman berikut :

`http://localhost:8080/api-docs/`

atau \
download file json API document dari link berikut:\
[download ](swagger.json)